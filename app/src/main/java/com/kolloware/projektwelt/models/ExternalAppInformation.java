package com.kolloware.projektwelt.models;

public class ExternalAppInformation {
    public final String appPackage;
    public final Integer appTitleResourceKey;

    public ExternalAppInformation(String inAppPackage, Integer inAppTitleResourceKey) {
        this.appPackage = inAppPackage;
        this.appTitleResourceKey = inAppTitleResourceKey;
    }
}
