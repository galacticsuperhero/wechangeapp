package com.kolloware.projektwelt.models;

import com.kolloware.projektwelt.utils.WechangeCookie;

import java.util.HashSet;
import java.util.Set;

import static com.kolloware.projektwelt.Constants.WECHANGE_URL;

public class ViewModel {

    public static String currentURL = WECHANGE_URL;
    public static WechangeCookie cookie = null;
    public static Set<Integer> notifiedIds = new HashSet<>();

    private ViewModel() {
    }
}
