package com.kolloware.projektwelt.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.kolloware.projektwelt.Constants;
import com.kolloware.projektwelt.models.ViewModel;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MarkNotificationsSeenTask extends AsyncTask<Void, Void, Void> implements Constants {

    private double timestamp;

    public MarkNotificationsSeenTask(double timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    protected Void doInBackground(Void[] objects) {
        try {
            String markReadURL = String.format(Locale.US,
                    "%s%.6f/", MARKSEEN_URL, timestamp);

            Log.v(LOG_NET, "markseen URL: " + markReadURL);

            if (ViewModel.cookie != null) {
                Map<String, String> headers = new HashMap<>();
                headers.put(HTTP_HEADER_COOKIE, ViewModel.cookie.toString());
                headers.put(HTTP_HEADER_CSFR_TOKEN, ViewModel.cookie.getCsrfToken());
                headers.put(HTTP_HEADER_REFERER, DASHBOARD_URL);

                HTTPUtils.executePostRequest(markReadURL, headers);
            }
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error marking entries as seen: " + e.getMessage());
            Log.i(LOG_NET, Log.getStackTraceString(e));
        }

        return null;
    }
}
