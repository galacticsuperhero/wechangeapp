package com.kolloware.projektwelt;

public interface Constants {

    /* Logging */
    String LOG_UI = "UI";
    String LOG_NET = "NET";
    String LOG_DATA = "DATA";
    String LOG_APP = "APP";

    /* URLs */
    String WECHANGE_URL = "https://projektwelt.drja.de/login";
    String NOTIFICATION_URL = "https://projektwelt.drja.de/profile/api/alerts/get/";
    String MARKSEEN_URL = "https://projektwelt.drja.de/profile/api/alerts/markseen/";
    String DASHBOARD_URL = "https://projektwelt.drja.de/dashboard/";
    String WECHANGE_MESSAGES_URL = "https://projektwelt.drja.de/messages/";

    /* Android packages */
    String APP_ROCKET_CHAT = "chat.rocket.android";

    /* Settings */
    long NOTIFICATIONS_REFRESH_INTERVAL = 10 * 60 * 1000;
    String CHANNEL_PLATFORM_NOTIFICATIONS_ID = "platform_notifications";
    int REQUEST_TIMER_EVERY_TEN_MINUTES = 1;

    /* Intent keys */
    String INTENT_KEY_URL = "url";
    String INTENT_KEY_TIMESTAMP = "timestampToMarkAsSeen";

    /* JSON notification data keys */
    String JSON_KEY_DATA = "data";
    String JSON_KEY_NEWEST_TIMESTAMP = "newest_timestamp";
    String JSON_KEY_ITEMS = "items";
    String JSON_KEY_IS_EMPHASIZED = "is_emphasized";
    String JSON_KEY_TEXT = "text";
    String JSON_KEY_GROUP = "group";
    String JSON_KEY_ID = "id";
    String JSON_KEY_URL = "url";

    /* HTTP header contants */
    String HTTP_HEADER_COOKIE = "Cookie";
    String HTTP_HEADER_CSFR_TOKEN = "X-CSRFToken";
    String HTTP_HEADER_REFERER = "Referer";
}
