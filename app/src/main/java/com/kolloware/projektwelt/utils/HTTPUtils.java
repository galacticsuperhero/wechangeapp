package com.kolloware.projektwelt.utils;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import static com.kolloware.projektwelt.BaseApplication.LOG_NET;

public class HTTPUtils {

    private static final String DEFAULT_CHARSET = "iso-8859-1";
    private static final int DEFAULT_INPUT_BUFFER_SIZE = 8;


    private static String getStringFromInputStream(InputStream is) throws Exception {
        Log.d(LOG_NET, HTTPUtils.class.getSimpleName() +
                ".getStringFromInputStream() called with: is = [" + is + "]");

        String result = null;

        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is, DEFAULT_CHARSET), DEFAULT_INPUT_BUFFER_SIZE);
            StringBuilder sb = new StringBuilder();

            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                sb.append(line);
                sb.append("\n");
            }

            is.close();
            result = sb.toString();
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.i(LOG_NET, Log.getStackTraceString(e));
            throw e;
        }

        return result;
    }

    public static JSONObject getJSONFromUrl(String url, Map<String, String> headers) throws Exception {
        Log.d("LOG_NET", HTTPUtils.class.getSimpleName()
                + ".getJSONFromUrl() called with: url = [" + url + "]");

        JSONObject result = null;

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);

            for (Map.Entry<String, String> forEntry : headers.entrySet()) {
                httpGet.addHeader(new BasicHeader(forEntry.getKey(), forEntry.getValue()));
            }

            HttpResponse httpResponse = httpClient.execute(httpGet);

            HttpEntity httpEntity = httpResponse.getEntity();
            InputStream is = httpEntity.getContent();

            String contentFromQuery = getStringFromInputStream(is);

            result = new JSONObject(contentFromQuery);
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.i(LOG_NET, Log.getStackTraceString(e));
            throw e;
        }

        return result;
    }

    public static void executePostRequest(String inUrl, Map<String, String> headers) throws Exception {
        Log.d(LOG_NET, HTTPUtils.class.getSimpleName() + ".executePostRequest() called with: "
                + "inUrl = [" + inUrl + "], headers = [" + headers + "]");

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(inUrl);

            for (Map.Entry<String, String> forEntry : headers.entrySet()) {
                httpPost.addHeader(new BasicHeader(forEntry.getKey(), forEntry.getValue()));
            }

            HttpResponse result = httpClient.execute(httpPost);

            InputStream is = result.getEntity().getContent();
            String stringResult = getStringFromInputStream(is);

            Log.v(LOG_NET, "Result: " + stringResult);
        }
        catch (Exception e) {
            Log.e(LOG_NET, "Error: " + e.getMessage());
            Log.i(LOG_NET, Log.getStackTraceString(e));
            throw e;
        }
    }
}
