package com.kolloware.projektwelt.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.kolloware.projektwelt.Constants;
import com.kolloware.projektwelt.R;
import com.kolloware.projektwelt.activities.MainActivity;
import com.kolloware.projektwelt.models.ViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PullNotificationsTask extends AsyncTask<Context, Void, Void> implements Constants {

    @Override
    protected Void doInBackground(Context... contexts) {

        if (ViewModel.cookie != null) {
            try {
                HTTPUtils parser = new HTTPUtils();

                Map<String, String> headers = new HashMap<>();
                headers.put(HTTP_HEADER_COOKIE, ViewModel.cookie.toString());

                JSONObject result = parser.getJSONFromUrl(
                        NOTIFICATION_URL,
                        headers);

                Log.i(LOG_NET, "Result: " + result.toString(2));

                Double newestTimestamp = result.getJSONObject(JSON_KEY_DATA)
                        .getDouble(JSON_KEY_NEWEST_TIMESTAMP);

                JSONArray array = result.getJSONObject(JSON_KEY_DATA).getJSONArray(JSON_KEY_ITEMS);
                for (int i = 0; i < array.length(); i++) {
                    JSONObject currentNotification = array.getJSONObject(i);
                    boolean notify = currentNotification.getBoolean(JSON_KEY_IS_EMPHASIZED);
                    if (notify) {
                        Log.i("LOG_NET", "Show this message: "
                                + currentNotification.getString(JSON_KEY_TEXT));

                        sendNotification(contexts[0], currentNotification, newestTimestamp);
                    }
                }

            } catch (Exception e) {
                Log.e("LOG_NET", "Error: " + e.getMessage());
                Log.i("LOG_NET", Log.getStackTraceString(e));
            }
        }


        return null;
    }


    private void sendNotification(Context context, JSONObject notificationData, Double newsetTimestamp) {
        Log.d(LOG_APP, getClass().getSimpleName()
                + ".sendNotification() called with: context = ["
                + context + "], notificationData = [" + notificationData + "]");

        try {
            int id = notificationData.getInt(JSON_KEY_ID);

            if (ViewModel.notifiedIds.contains(id)) {
                Log.d(LOG_APP, "Already notified ID: " + id);
                return;
            }

            String url = notificationData.getString(JSON_KEY_URL);
            String details = notificationData.getString(JSON_KEY_TEXT);
            String group = notificationData.getString(JSON_KEY_GROUP);
            int priority = NotificationCompat.PRIORITY_DEFAULT;

            String contentTitle = context.getString(R.string.notification_new_activity, group);
            String contentText = Html.fromHtml(details).toString();

            Intent intent = new Intent(context, MainActivity.class);
            intent.putExtra(INTENT_KEY_URL, url);
            intent.putExtra(INTENT_KEY_TIMESTAMP, newsetTimestamp);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, id, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Notification notification = new NotificationCompat.Builder(context,
                    CHANNEL_PLATFORM_NOTIFICATIONS_ID)
                    .setSmallIcon(R.drawable.icon_bell)
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)
                    .setContentIntent(pendingIntent)
                    .setPriority(priority)
                    .setCategory(NotificationCompat.CATEGORY_SOCIAL)
                    .setAutoCancel(true)
                    .build();

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

            notificationManager.notify(id, notification);

            ViewModel.notifiedIds.add(id);
        }
        catch (Exception e) {
            Log.e(LOG_APP, "Error: " + e.getMessage());
            Log.i(LOG_APP, Log.getStackTraceString(e));
        }
    }
}
