package com.kolloware.projektwelt;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.kolloware.projektwelt.utils.PullNotificationsReceiver;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class BaseApplication extends Application implements Constants {


    @Override
    public void onCreate() {
        Log.d(LOG_APP, "BaseApplication.onCreate() called");

        super.onCreate();

        createNotificationChannels();
        initAlarmReceiver();
    }

    private void createNotificationChannels() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".createNotificationChannels() called");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_PLATFORM_NOTIFICATIONS_ID,
                    getString(R.string.notification_info_title),
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            channel1.setDescription(getString(R.string.notification_info_description));

            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(channel1);
            }
        }
    }

    private void initAlarmReceiver() {
        Log.d(LOG_APP, getClass().getSimpleName() + ".initAlarmReceiver() called");

        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        if (manager != null) {
            Intent dailyAlarmIntent = new Intent(this, PullNotificationsReceiver.class);
            PendingIntent pendingDailyAlarmIntent = PendingIntent.getBroadcast(this,
                    REQUEST_TIMER_EVERY_TEN_MINUTES, dailyAlarmIntent, FLAG_UPDATE_CURRENT);
            manager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                    NOTIFICATIONS_REFRESH_INTERVAL, pendingDailyAlarmIntent);
        }
    }

    public static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }
}
