package com.kolloware.projektwelt.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kolloware.projektwelt.Constants;

public class PullNotificationsReceiver extends BroadcastReceiver implements Constants {

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d(LOG_APP, getClass().getSimpleName() + ".onReceive() called with: context = ["
                + context + "], intent = [" + intent + "]");

        new PullNotificationsTask().execute(context);
    }

}
