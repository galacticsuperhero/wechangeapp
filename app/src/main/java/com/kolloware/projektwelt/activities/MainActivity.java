package com.kolloware.projektwelt.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;

import com.kolloware.projektwelt.Constants;
import com.kolloware.projektwelt.BaseApplication;
import com.kolloware.projektwelt.Constants;
import com.kolloware.projektwelt.models.ExternalAppInformation;
import com.kolloware.projektwelt.utils.MarkNotificationsSeenTask;
import com.kolloware.projektwelt.R;
import com.kolloware.projektwelt.models.ViewModel;
import com.kolloware.projektwelt.utils.WechangeCookie;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements Constants {

    private WebView browser;
    private ProgressBar progressBar;

    private ActionMenuItemView actionBack;
    private ActionMenuItemView actionForward;

    private Map<String, ExternalAppInformation> appsToLaunchByURL;

    public MainActivity() {
        appsToLaunchByURL = new HashMap<>();
        appsToLaunchByURL.put(WECHANGE_MESSAGES_URL,
                new ExternalAppInformation(APP_ROCKET_CHAT, R.string.app_title_rocket_chat));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d(LOG_UI, getClass().getSimpleName()
                + ".onNewIntent() called with: intent = [" + intent + "]");

        Bundle intentExtras = intent.getExtras();
        Double newestTimestamp = null;

        if (intentExtras != null) {
            String urlFromIntent = intentExtras.getString(INTENT_KEY_URL);
            Log.v(LOG_APP, "url from intent: " + urlFromIntent);
            ViewModel.currentURL = urlFromIntent;
            newestTimestamp = intentExtras.getDouble(INTENT_KEY_TIMESTAMP);
            (new MarkNotificationsSeenTask(newestTimestamp)).execute();
        }

        browser.loadUrl(ViewModel.currentURL);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_UI, getClass().getSimpleName() + ".onCreate() called with: savedInstanceState = ["
                + savedInstanceState + "]");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        progressBar = findViewById(R.id.progressbar);

        browser = findViewById(R.id.webview);
        browser.getSettings().setLoadsImagesAutomatically(true);
        browser.getSettings().setJavaScriptEnabled(true);
        browser.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        browser.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (appsToLaunchByURL.containsKey(url)) {
                    Log.v(LOG_NET, "URL found. Starting external app");
                    ExternalAppInformation appInfo = appsToLaunchByURL.get(url);

                    boolean startedSuccessfully = BaseApplication.openApp(getApplicationContext(),
                            appInfo.appPackage);

                    if (!startedSuccessfully) {
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.message_app_not_found, getString(appInfo.appTitleResourceKey)),
                                Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    view.loadUrl(url);
                }

                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressBar(true);
                ViewModel.currentURL = url;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                showProgressBar(false);
                updateNavigationVisibilities();
            }
        });

        browser.loadUrl(ViewModel.currentURL);

        String cookieString = CookieManager.getInstance().getCookie(ViewModel.currentURL);

        if (cookieString != null && !cookieString.isEmpty()) {
            ViewModel.cookie = new WechangeCookie(CookieManager.getInstance().getCookie(ViewModel.currentURL));
            Log.i(LOG_NET, "Cookie: " + ViewModel.cookie);
        }
        else {
            ViewModel.cookie = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    private int getVisibilityByBoolean(boolean visible) {
        return visible ? View.VISIBLE : View.INVISIBLE;
    }

    private void updateNavigationVisibilities() {
        if (actionBack == null) { actionBack = findViewById(R.id.action_back); }
        if (actionForward == null) { actionForward = findViewById(R.id.action_forward); }

        if (actionBack == null) return;

        actionBack.setVisibility(getVisibilityByBoolean(browser.canGoBack()));
        actionForward.setVisibility(getVisibilityByBoolean(browser.canGoForward()));
    }

    private void showProgressBar(boolean visible) {
        if (visible) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setIndeterminate(true);
        }
        else {
            progressBar.setVisibility(View.GONE);
            updateNavigationVisibilities();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_back:
                if (browser.canGoBack()) {
                    browser.goBack();
                }
                break;
            case R.id.action_forward:
                if (browser.canGoForward()) {
                    browser.goForward();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && browser.canGoBack()) {
            browser.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}
